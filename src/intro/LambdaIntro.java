package intro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static junit.framework.Assert.assertEquals;
import org.junit.Ignore;
import org.junit.Test;

/**
 * jorgen.x.andersson@capgemini.com
 * @se_thinking
 * http://se-thinking.blogspot.se
 */
public class LambdaIntro {

    @Test
    public void whatIsAJava8Lambda() {

        // java.util.function
        Function<Integer, String> integerToString = i -> i.toString();

        BiFunction<Integer, Integer, Integer> addition = (a, b) -> a + b;

        IntFunction<String> intAddtion = i -> Integer.toString(i);

        Predicate<Integer> moreThanFive = i -> i > 5;

        MyLambda noArgLambda = () -> System.out.println("Hello Lambda");

        MyLambda codeBlock = () -> {
            int i = 0;
            i++;
            i = i + 2;
            System.out.println(i);
        };

    }

    private static final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    @Test @Ignore
    public void whatIsAStream() {

        // java.util.stream
        Stream<Integer> stream = list.stream();

        // Intermediate methods
        IntStream intStream
                = stream.filter(i -> i > 5)
                .limit(6)
                .skip(1)
                .map(i -> i * 10) //Stream<Integer>
                .map(i -> i.toString()) //Stream<String>
                .flatMapToInt(s -> s.chars()); //IntStream

        // Terminal methods
        stream.count();
        stream.collect(Collectors.groupingBy(i -> i % 4));
        stream.forEach(i -> System.out.println(i));

    }

    // Other ways to use lambdas
    private static interface MyLambda {

        public void myLambdaMethod();
    }

    private static class MyLambdaExecutor {

        public static void execute(MyLambda myLambda) {
            myLambda.myLambdaMethod();
        }
    }

    @Test
    public void testAPIMedLambda() {
        MyLambdaExecutor.execute(() -> System.out.println("Hello MyLambda"));
    }

    @Test
    public void testMyFreeLambda() {
        MyLambda myFreeLambda = () -> System.out.println("Hello MyLambda");

        MyLambdaExecutor.execute(myFreeLambda);
    }

    private static void myLambdaAsMethod() {
        System.out.println("Hello MyLambda");
    }

    @Test
    public void testMyFreeLambdaFromMethodHandle() {
        MyLambda myFreeLambda = LambdaIntro::myLambdaAsMethod;

        MyLambdaExecutor.execute(myFreeLambda);
    }

    @Test
    public void testMethodhandleWithParameter() {
        list.forEach(System.out::println);
    }

    // Java 8 Lambda under the hood
    @Test
    public void testVanillaAnonymousClass() {
        MyLambdaExecutor.execute(new MyLambda() {

            @Override
            public void myLambdaMethod() {
                System.out.println(getClass().toGenericString());
            }
        });
    }

    // Re-use in functional programming
    @Test
    public void partiallyAppliedFunction() {
        IntBinaryOperator multiplier = (l, r) -> l * r;
        IntFunction doubler = i -> multiplier.applyAsInt(i, 2);

        assertEquals(6, doubler.apply(3));
    }

    private static interface IntTernaryOperator {

        public int applyAsInt(int a, int b, int c);
    }

    @Test
    public void functionalVolumeAndAreaCalculation() {
        IntTernaryOperator volume = (x, y, z) -> x * y * z;
        IntBinaryOperator area = (x, y) -> volume.applyAsInt(x, y, 1);

        assertEquals(24, volume.applyAsInt(2, 3, 4));
        assertEquals(6, area.applyAsInt(2, 3));
    }

    
    
    // Closure over an outside variable
    @Test
    public void closure() {
        int variableOutside = 0;
        MyLambda assertLambda = () -> assertEquals(0, variableOutside);

//        variableOutside = 1;
        MyLambdaExecutor.execute(assertLambda);
    }

    @Test
    public void closureWithMutableObject() {
        MutableValue variableOutside = new MutableValue();
        MyLambda assertLambda = () -> assertEquals(1, variableOutside.value);

        variableOutside.value = 1;
        MyLambdaExecutor.execute(assertLambda);
    }

    private static class MutableValue {

        public int value = 0;
    }

    @Test
    public void countValuesInAStream() {
        MutableValue counter = new MutableValue();

        final List<String> upperCaseList = Arrays.asList("a", "b", "c").stream()
                .peek(s -> counter.value++)
                .map(s -> s.toUpperCase())
                .collect(toList());

        assertEquals(upperCaseList.size(), counter.value);
    }

    //Comparison to iteration where the stream is generated.
    @Test
    public void iterateOverIntsAndConvertToHex() {
        final int lengthOfList = 256;
        final List<String> printoutStrings = new ArrayList<>(lengthOfList);
        for (int i = 0; i < lengthOfList; i++) {
            final String printoutString = i + " -> " + Integer.toHexString(i).toUpperCase();
            printoutStrings.add(printoutString);
//            System.out.println(printoutString);
        }

        assertEquals(lengthOfList, printoutStrings.size());
    }

    @Test
    public void generateStreamOfIntsAndConvertToHex() {
        final int lengthOfList = 256;
        final List<String> printoutStrings = Stream.iterate(0, i -> i++).limit(lengthOfList)
                .map(i -> i + " -> " + Integer.toHexString(i))
                .map(s -> s.toUpperCase())
                .collect(toList());

        assertEquals(lengthOfList, printoutStrings.size());

//        printoutStrings.stream().forEach(System.out::println);
    }
    
    /* A little bit more on Functional Programming
    
    // Great talk bridging FP and OOP - Might requrire a free subscription
    https://skillsmatter.com/skillscasts/5070-twins-fp-and-oop
    
    // Great (written) introducion to FP concepts by Neal Ford
    http://nealford.com/functionalthinking.html
    */
}